# Cucumdroid Cucumber Runner library #
This is main library for Cucumdroid. This is the heart of the cucumber runner.  It's responsible for parsing the compiles test classes and feature files.  It will then execute the discovered features and output the results.


## Usage

Tests should be run via the 'uitest' ant target. This will 

 - copy the cucumber runner jar to the phone
 - dex the supplied test jar file
 - copy the dexed test jar onto the phone
 - execute the cucumber runner to run the tests
 - copy the results file from the device


### The test jar ###
This is the file that will contain the actual tests.
The library will look at all the annotated source files and build a list of all the executable cucumber statements.

It will then look for

	- resource/manifest.feature
in here should be a list of all the .feature files in

	- resource/
that the cucumber runner should execute. 

----------


### This library requires ###
Android API 17

Android uiAutomator.jar

[Cucumdroid annotations](https://bitbucket.org/proxama/android_cucumdroid_annotations/overview "Cucumdroid")


gson-2.2.2.jar

Gherkin

----------
### License ###
This work is licensed under a [Creative Commons Attribution-ShareAlike 3.0 Unported License](http://creativecommons.org/licenses/by-sa/3.0/deed.en_GB "details").


