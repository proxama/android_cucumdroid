/**
 * This work is licensed under a Creative Commons Attribution-ShareAlike 3.0 Unported License.
 * http://creativecommons.org/licenses/by-sa/3.0/deed.en_GB
 * Main package for the Android implementation of a cucumber test tool.
**/
package com.proxama.android.cucumdroid;