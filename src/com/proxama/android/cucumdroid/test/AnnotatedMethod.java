/**
 * This work is licensed under a Creative Commons Attribution-ShareAlike 3.0 Unported License.
 * http://creativecommons.org/licenses/by-sa/3.0/deed.en_GB
**/
package com.proxama.android.cucumdroid.test;

import java.lang.reflect.Method;

/**
 * Holds a Method and the text of it's annotation.
 * @author Stephen Charman
 *
 */
public class AnnotatedMethod {

	
	/**
	 * The text of the annotation.
	 */
	private String mAnnotation;
	
	/**
	 * The method which had the annotation. 
	 */
	private Method mMethod;

	/**
	 * Creates a new instance.
	 * @param annotation the annotation text.
	 * @param method the method that was annotated.
	 */
	public AnnotatedMethod(final String annotation, final Method method) {
		mAnnotation = annotation;
		mMethod = method;
	}
	
	/**
	 * Get the text of the annotation.
	 * @return the annotation text; otherwise null.
	 */
	public String getAnnotation() {
		return mAnnotation;
	}
	
	/**
	 * Gets the method that was annotated.
	 * @return the method.
	 */
	public Method getMethod() {
		return mMethod;
	}
}
