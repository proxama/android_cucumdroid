/**
 * This work is licensed under a Creative Commons Attribution-ShareAlike 3.0 Unported License.
 * http://creativecommons.org/licenses/by-sa/3.0/deed.en_GB
 * Package containing files for loading and reading classes and methods which are annotated for cucumber testing.
**/
package com.proxama.android.cucumdroid.test;