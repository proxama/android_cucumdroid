/**
 * This work is licensed under a Creative Commons Attribution-ShareAlike 3.0 Unported License.
 * http://creativecommons.org/licenses/by-sa/3.0/deed.en_GB
**/
package com.proxama.android.cucumdroid.test;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.proxama.android.cucumdroid.annotation.Annotation.And;
import com.proxama.android.cucumdroid.annotation.Annotation.Given;
import com.proxama.android.cucumdroid.annotation.Annotation.Then;
import com.proxama.android.cucumdroid.annotation.Annotation.When;

import dalvik.system.DexFile;
import dalvik.system.PathClassLoader;

/**
 * Class for parsing a dex file to extract it's methods where they're annotated like Cucumber tests.
 * @author Stephen Charman
 *
 */
public class ClassParser {

	/**
	 * List of the annotated methods found.
	 */
	private List<AnnotatedMethod> mAnnotatedMethods = new ArrayList<AnnotatedMethod>();
	
	/**
	 * Loads and parses all the class in a dex file to extract the annotated methods.
	 * @param dexLocation location of the dexed jar to load and parse.
	 * @param classLoader a classloader for loading found classes.
	 */
	public void parseDexJar(final String dexLocation, final ClassLoader classLoader) {
		PathClassLoader pathLoader = new PathClassLoader(dexLocation, classLoader);
		
		try {
			DexFile dexFile = new DexFile(dexLocation);
			Enumeration<String> entries = dexFile.entries();

			while (entries.hasMoreElements()) {
				String entry = entries.nextElement();
				try {
					parseClass(pathLoader.loadClass(entry));
				} catch (ClassNotFoundException e) {
					e.printStackTrace();
				}				
			}
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}
	
	/**
	 * Uses regex to attempt to match a known {@link AnnotatedMethod} against a given piece of text.
	 * @param annotation the annotation text to look for.
	 * @return {@link CucumberMethod} containing the Method and any parameters; otherwise null.
	 */
	public CucumberMethod findMethodForAnnotation(final String annotation) {
		CucumberMethod foundMethod = null;
		
		for (AnnotatedMethod method : mAnnotatedMethods) {
			Pattern regexPattern = Pattern.compile(method.getAnnotation());
			Matcher matcher = regexPattern.matcher(annotation);
			
			if (matcher.matches()) {
				int groupCount = matcher.groupCount();
				String[] groups = new String[groupCount];
				for (int x = 0; x < groupCount; x++) {
					groups[x] = matcher.group(x + 1);
				}
				foundMethod = new CucumberMethod(method.getMethod(), groups);
				break;
			}
		}
		
		return foundMethod;
	}
	
	/**
	 * Looks at a given Class to find it's annotated methods.
	 * @param clazz the class file to inspect.
	 */
	private void parseClass(final Class<?> clazz) {
		
		Method[] methods = clazz.getMethods();
		String annotation = null;
		
		for (Method method : methods) {
			annotation = null;
			
			if (method.isAnnotationPresent(Given.class)) {
				Given given = method.getAnnotation(Given.class);
				annotation = given.value();
			} else if (method.isAnnotationPresent(When.class)) {
				When when = method.getAnnotation(When.class);
				annotation = when.value();
			} else if (method.isAnnotationPresent(Then.class)) {
				Then then = method.getAnnotation(Then.class);
				annotation = then.value();
			} else if (method.isAnnotationPresent(And.class)) {
				And and = method.getAnnotation(And.class);
				annotation = and.value();
			}
			
			if (annotation != null) {
				mAnnotatedMethods.add(new AnnotatedMethod(annotation, method));
			}
		}
	}	
}
