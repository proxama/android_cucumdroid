/**
 * This work is licensed under a Creative Commons Attribution-ShareAlike 3.0 Unported License.
 * http://creativecommons.org/licenses/by-sa/3.0/deed.en_GB
**/
package com.proxama.android.cucumdroid.test;

import java.lang.reflect.Method;

/**
 * Holds a Method and it's parameters so that it can be invoked correctly.
 * @author Stephen Charman
 *
 */
public class CucumberMethod {

	/**
	 * The Method for this test.
	 */
	private Method mMethod;
	
	/**
	 * Any parameters for this method. 
	 */
	private Object[] mParameters = null;

	/**
	 * Creates a new instance of this class.
	 * @param method the Method that relates to a cucumber Step statement.
	 * @param parameters any parameters found for the method.
	 */
	public CucumberMethod(final Method method, final Object[] parameters) {
		mMethod = method;
		mParameters = parameters;
	}
	
	/**
	 * Gets the method.
	 * @return the method.
	 */
	public Method getMethod() {
		return mMethod;
	}
	
	/**
	 * Get the parameters for this Method.
	 * @return an array of the parameters or null.
	 */
	public Object[] getParameters() {
		return mParameters;
	}
}
