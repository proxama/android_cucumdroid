/**
 * This work is licensed under a Creative Commons Attribution-ShareAlike 3.0 Unported License.
 * http://creativecommons.org/licenses/by-sa/3.0/deed.en_GB
**/
package com.proxama.android.cucumdroid;

import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xmlpull.v1.XmlSerializer;

import android.os.Bundle;
import android.util.Xml;

import com.android.uiautomator.testrunner.UiAutomatorTestCase;
import com.proxama.android.cucumdroid.dill.FeatureParser;
import com.proxama.android.cucumdroid.dill.model.Feature;
import com.proxama.android.cucumdroid.dill.model.Scenario;
import com.proxama.android.cucumdroid.dill.model.Step;
import com.proxama.android.cucumdroid.test.ClassParser;
import com.proxama.android.cucumdroid.test.CucumberMethod;
import com.proxama.android.cucumdroid.testcase.CucumdroidTestCase;

/**
 * @author Stephen Charman
 *
 */
public class CucumberRunner extends UiAutomatorTestCase {
	
	/**
	 * Constant for outputting the results in xml format.
	 */
	private static final String OUTPUT_XML = "xml";
	
	
	/**
	 * Constant for outputting the results in json format.
	 */
	private static final String OUTPUT_JSON = "json";
	
	/**
	 * Cache of previously created classes.
	 */
	private HashMap<Class<?>, Object> previouslyInstantiatedClasses = new HashMap<Class<?>, Object>();
	
	/**
	 * List of the parsed {@link Feature}s. 
	 */
	private List<Feature> mFeatures = new ArrayList<Feature>();
	
	/**
	 * Root test, this method begins the process of loading and parsing the cucumber features and
	 * matching annotated methods and then exercising the tests.
	 */
	public void testExecutor() {
	
		String testsDirectory = getTestFilesLocation();
		if (testsDirectory != null) {
			testsDirectory += "dexed_tests.jar";
			final ClassLoader loader = CucumberRunner.class.getClassLoader();
			final ClassParser classParser = new ClassParser();
			
			classParser.parseDexJar(testsDirectory, loader);
			mFeatures = FeatureParser.parseResources(testsDirectory, loader);
			
			countSenarios(mFeatures);
			try {
				runTests(mFeatures, classParser);
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (InstantiationException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				e.printStackTrace();
			}
		} else {
			System.out.println("Unable to find 'testsdir' in params");
		}

	}
	
	@Override
	protected void tearDown() throws Exception {
		super.tearDown();

		final String testsDir = getTestFilesLocation();
		if (testsDir != null) {
			final String outputType = getOutputType();
			
			if (OUTPUT_XML.equals(outputType)) {
				System.out.println("Creating XML results file");
				writeXml(testsDir + "/results.xml");
			} else {
				System.out.println("Creating JSON results file");
				writeJson(testsDir + "/results.json");
			}
			
			System.out.println("Results file saved");
		} else {
			System.out.println("Unable to find 'testsdir' in params");
		}
	}
	
	/**
	 * Gets the supplied 'testsdir' param from the command line input.
	 * @return the value; otherwise null.
	 */
	private String getTestFilesLocation() {
		return getCommandLineValue("testsdir", null);
	}
	
	/**
	 * Attempts to returns the selected output type for results.
	 * @return the type specified by the command line; otherwise json.
	 */
	private String getOutputType() {
		return getCommandLineValue("outputType", OUTPUT_JSON);
	}
	
	/**
	 * Gets a command line value from the params passed to the test.
	 * @param key to get the value for
	 * @param defaultValue the default value if no params or no key.
	 * @return the value; otherwise defaultValue.
	 */
	private String getCommandLineValue(final String key, final String defaultValue) {
		String result = defaultValue;
		
		Bundle params = getParams();
		
		if (params != null) {
			result = params.getString(key, defaultValue);	
		}
		
		return result;
	}
	
	/**
	 * Loops over all the features, scenarios and steps.
	 * @param features The parsed {@link Feature} to be tested.
	 * @param classParser The {@link ClassParser} that contains all the found code-tests.
	 * @throws InstantiationException if there is an error loading the class.
	 * @throws IllegalAccessException if there is an error loading the class.
	 * @throws InvocationTargetException if there was error when running the test method.
	 */
	private void runTests(final List<Feature> features, final ClassParser classParser) 
			throws InstantiationException, IllegalAccessException, InvocationTargetException {
	
		for (Feature feature : features) {
			
			System.out.println("=====================================");
			System.out.println("Testing feature: " + feature.getTitle());

			for (Scenario scenario : feature.getScenarios()) {

				System.out.println("Testing scenario: " + scenario.getTitle());
				
				for (Step step : scenario.getSteps()) {
					CucumberMethod testMethod = classParser.findMethodForAnnotation(step.getText());
					if (testMethod != null) {
						Object parent = getParentClass(testMethod.getMethod().getDeclaringClass());
						testMethod.getMethod().invoke(parent, testMethod.getParameters());
						step.setSuccess(true);
					} else {
						fail("Unable to find method for step: " + step.getText());
					}
				}
				
				System.out.println("-------------------------------------");
			}
		}
	}
	
	
	/**
	 * Gets the class for a given method. If the Class has not been got before it will create a new instance.
	 * It will then call setUiDevice().
	 * @param clazz the class to return an instance of.
	 * @return The class instance for the class.
	 * @throws InstantiationException if there is an error loading the class.
	 * @throws IllegalAccessException if there is an error loading the class.
	 */
	private Object getParentClass(final Class<?> clazz) throws InstantiationException, IllegalAccessException {
		if (!previouslyInstantiatedClasses.containsKey(clazz)) {
			CucumdroidTestCase instance = (CucumdroidTestCase) clazz.newInstance();
			instance.setUiDevice(getUiDevice());
			
			previouslyInstantiatedClasses.put(clazz, instance);
		}
		
		return previouslyInstantiatedClasses.get(clazz);
	}
	
	/**
	 * Prints out some information about the features found into the console.
	 * @param features the features to be tested.
	 */
	private void countSenarios(final List<Feature> features) {
		System.out.println(String.format("Found %d features", features.size()));
		int scenarios = 0;
		for (Feature feature : features) {
			scenarios += feature.getScenarios().size();
		}
		
		System.out.println(String.format("Found %d scenarios", scenarios));
	}
	
	/**
	 * Writes the results to a json file.
	 * @param fileAndPath the full filename and path where to output the file.
	 * @throws IOException if there is an error.
	 * @throws JSONException if there is an error.
	 */
	private void writeJson(final String fileAndPath) throws IOException, JSONException {
		
		JSONObject testResults = new JSONObject();
		testResults.put("time", Calendar.getInstance().getTime().toString());
		testResults.put("featuresCount", mFeatures.size());
		
		JSONArray features = new JSONArray();
		
		for (Feature feature : mFeatures) {
			features.put(feature.getJson());
		}
		
		testResults.put("features", features);
		
		FileWriter fileWriter = new FileWriter(fileAndPath);
		fileWriter.write(testResults.toString());
		fileWriter.close();
	}
	
	/**
	 * Writes the results to a xml file.
	 * @param fileAndPath the full filename and path where to output the file.
	 * @throws IOException if there is an error.
	 * @throws JSONException
	 */	
	private void writeXml(final String fileAndPath) throws IOException {
		FileWriter xmlWriter = new FileWriter(fileAndPath);
		XmlSerializer serializer = Xml.newSerializer();
		serializer.setOutput(xmlWriter);
		
		serializer.startDocument("UTF-8", true);
        serializer.startTag("", "testsuites");

		for (Feature feature : mFeatures) {
			feature.getXml(serializer);
		}
			
		serializer.endTag("", "testsuites");
		serializer.endDocument();
		xmlWriter.close();
	}
}
