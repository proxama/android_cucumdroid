/**
 * This work is licensed under a Creative Commons Attribution-ShareAlike 3.0 Unported License.
 * http://creativecommons.org/licenses/by-sa/3.0/deed.en_GB
**/
package com.proxama.android.cucumdroid.dill;

import gherkin.lexer.I18nLexer;
import gherkin.lexer.Lexer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import com.proxama.android.cucumdroid.dill.model.Feature;


import dalvik.system.PathClassLoader;


/**
 * Class for loading a dex file, reading it's features.manifest from /resources and 
 * then parsing all the .feature files it lists Cucumber tests.
 * @author Stephen Charman
 *
 */
public abstract class FeatureParser {
	  
	/**
	 * Private constructor to prevent instantiation.
	 */
	private FeatureParser() { }
	
	/** Loads a dex file, enumerating it's methods and extracting the ones annotated for Cucumber tests.
	 * @param dexLocation the location of the Dex file to load.
	 * @param classLoader The classloader for instantiating the classes.
	 * @return a list of {@link Feature}s found; otherwise an empty list.
	 */
	public static final List<Feature> parseResources(final String dexLocation, final ClassLoader classLoader) {
		List<Feature> features = new ArrayList<Feature>();
		
		PathClassLoader pathLoader = new PathClassLoader(dexLocation, classLoader);
		try {
			InputStreamReader stream = new InputStreamReader(pathLoader.getResourceAsStream("features.manifest"));
			BufferedReader bufferedReader = new BufferedReader(stream);
			
			try {
				List<String> manifestEntries = new ArrayList<String>();
				String entry = "";
				while ((entry = bufferedReader.readLine()) != null) {
					manifestEntries.add(entry);
				}
				features = readFeatureFiles(pathLoader, manifestEntries);				
			} finally {
			
				bufferedReader.close();
				stream.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return features;
	}

	/**
	 * Loops over files in the manifest and parses them for cucumber statements.
	 * @param pathLoader contains the files to be opened and parsed.
	 * @param manifestEntries the list of files to be parsed.
	 * @return a list of {@link Feature}s extracted; otherwise an empty list.
	 */
	private static List<Feature> readFeatureFiles(final PathClassLoader pathLoader, 
			final List<String> manifestEntries) {
		SimpleListener featureListener = new SimpleListener();
		
		for (String entry : manifestEntries) {
			pathLoader.getResourceAsStream(entry);
			
			String featureData = extractData(pathLoader.getResourceAsStream(entry));

			Lexer lexer = new I18nLexer(featureListener); 
			lexer.scan(featureData);
			
		}
		return featureListener.getFeatures();
	}

	/**
	 * Reads all the lines from a given input stream.
	 * @param resourceAsStream the stream to read.
	 * @return a String of all the text; otherwise an empty string.
	 */
	private static String extractData(final InputStream resourceAsStream) {
		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(resourceAsStream));
		
		StringBuilder builder = new StringBuilder();
		String entry = "";
		try {
			
			while ((entry = bufferedReader.readLine()) != null) {
				builder.append(entry);
				builder.append("\n");
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (bufferedReader != null) {
				try {
					bufferedReader.close();
				} catch (IOException e) { 
					e.printStackTrace();
				}
			}
		}
		
		return builder.toString();
	}
}
