/**
 * This work is licensed under a Creative Commons Attribution-ShareAlike 3.0 Unported License.
 * http://creativecommons.org/licenses/by-sa/3.0/deed.en_GB
**/
package com.proxama.android.cucumdroid.dill;

import gherkin.lexer.Listener;

import java.util.ArrayList;
import java.util.List;

import com.proxama.android.cucumdroid.dill.model.Feature;
import com.proxama.android.cucumdroid.dill.model.Scenario;
import com.proxama.android.cucumdroid.dill.model.Step;


/**
 * A simple listener used by Gherkin when it's paring a .feature file.
 * This is used to build up the {@link Feature} {@link Scenario} and {@link Step} classes.
 * @author Stephen Charman
 *
 */
public class SimpleListener implements Listener {

	/**
	 * List of all the created {@link Feature}s.
	 */
	private List<Feature> mFeatures = new ArrayList<Feature>();
	
	/**
	 * The current {@link Feature} being constructed. 
	 */
	private Feature mCurrentFeature;
	
	/**
	 * The current {@link Scenario} being constructed.
	 */
	private Scenario mCurrentScenario;
	
	/**
	 * Gets all the features found when parsing.
	 * @return A list of {@link Feature}; otherwise false.
	 */
	public List<Feature> getFeatures() {
		return mFeatures;
	}
	
	@Override
	public void background(final String keyword, final String name, final String description, final Integer line) {

	}

	@Override
	public void comment(final String comment, final Integer line) {

	}

	@Override
	public void docString(final String arg0, final String arg1, final Integer arg2) {

	}

	@Override
	public void eof() {

	}

	@Override
	public void examples(final String keyword, final String name, final String description, final Integer line) {
	}

	@Override
	public void feature(final String keyword, final String name, final String description, final Integer line) {
		mCurrentFeature = new Feature(name);
		mFeatures.add(mCurrentFeature);
	}

	@Override
	public void row(final List<String> arg0, final Integer arg1) {
	}

	@Override
	public void scenario(final String keyword, final String name, final String description, final Integer line) {
		mCurrentScenario = new Scenario(name);
		mCurrentFeature.addScenario(mCurrentScenario);
	}

	@Override
	public void scenarioOutline(final String keyword, final String name, final String description, final Integer line) {
		mCurrentScenario = new Scenario(name);
		mCurrentFeature.addScenario(mCurrentScenario);
	}

	@Override
	public void step(final String keyword, final String name, final Integer line) {
		mCurrentScenario.addStep(new Step(name));
	}

	@Override
	public void tag(final String tag, final Integer line) {
	}

}
