/**
 * This work is licensed under a Creative Commons Attribution-ShareAlike 3.0 Unported License.
 * http://creativecommons.org/licenses/by-sa/3.0/deed.en_GB
**/
package com.proxama.android.cucumdroid.dill.model;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xmlpull.v1.XmlSerializer;

/**
 * Class representing  a Cucumber Feature.
 * @author Stephen Charman
 *
 */
public class Feature {

	/**
	 * The title for the feature.
	 */	
	private String mTitle;
	
	/**
	 * The {@link Scenario}s associated with this Feature. 
	 */	
	private List<Scenario> mScenarios = new ArrayList<Scenario>();
	
	/**
	 * Creates a new instance.
	 * @param title the title of this feature.
	 */
	public Feature(final String title) {
		mTitle = title;
	}
	
	/**
	 * Gets the title of this feature.
	 * @return the title.
	 */
	public String getTitle() {
		return mTitle;
	}
	
	/**
	 * Gets all the {@link Scenario}s for this feature.
	 * @return A list of Scenario; otherwise an empty list. 
	 */
	public List<Scenario> getScenarios() {
		return mScenarios;
	}

	/**
	 * Adds a given {@link Scenario} to the feature.
	 * @param scenario the scenario to add.
	 */
	public void addScenario(final Scenario scenario) {
		mScenarios.add(scenario);
	}
	
	/**
	 * Indicates if all the {@link Scenario}s for this feature are successful.
	 * @return true if all the scenarios are successful; otherwise false.
	 */
	public boolean isSuccessful() {
		boolean success = true;
		for (Scenario scenario : mScenarios) {
			if (!scenario.isSuccessful()) {
				success = false;
				break;
			}
		}
		
		return success;
	}
	
	/**
	 * Gets a json representation of this feature and all it's child {@link Scenario}s.
	 * @return the constructed json object.
	 * @throws JSONException if tere is an error in building the json.
	 */	
	public JSONObject getJson() throws JSONException {
		JSONObject json = new JSONObject();
		json.put("name", mTitle);
		json.put("scenariosCount", mScenarios.size());
		json.put("successful", isSuccessful());
		
		JSONArray scenarios = new JSONArray();
		
		for (Scenario step : mScenarios) {
			scenarios.put(step.getJson());
		}
		
		json.put("scenarios", scenarios);
		
		return json;
	}
	
	/** writes an xml line for this feature.
	 * @param serializer the serializer to write into.
	 * @throws IOException on an error.
	 */
	public void getXml(final XmlSerializer serializer) throws IOException {
		serializer.startTag("", "testsuite");
		serializer.attribute("", "name", mTitle);
		serializer.attribute("", "tests", String.valueOf(mScenarios.size()));
		serializer.attribute("", "disabled", "0");
		serializer.attribute("", "errors", "0");
		serializer.attribute("", "time", "0");	
		serializer.attribute("", "classname", mTitle);

		int failureCount = 0;
		for (Scenario step : mScenarios) {
			if (!step.isSuccessful()) {
				failureCount++;
			}
		}
	
		serializer.attribute("", "failures", String.valueOf(failureCount));
		for (Scenario step : mScenarios) {
			if (!step.isSuccessful()) {
				step.getXml(serializer);
				break;
			}
		}		
		serializer.endTag("", "testsuite");
	}
}
