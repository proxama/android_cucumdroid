/**
 * This work is licensed under a Creative Commons Attribution-ShareAlike 3.0 Unported License.
 * http://creativecommons.org/licenses/by-sa/3.0/deed.en_GB
 * Package containing classes to represent different parts of a parsed gherkin file.
**/
package com.proxama.android.cucumdroid.dill.model;