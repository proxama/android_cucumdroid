/**
 * This work is licensed under a Creative Commons Attribution-ShareAlike 3.0 Unported License.
 * http://creativecommons.org/licenses/by-sa/3.0/deed.en_GB
**/
package com.proxama.android.cucumdroid.dill.model;

import java.io.IOException;

import org.json.JSONException;
import org.json.JSONObject;
import org.xmlpull.v1.XmlSerializer;

/**
 * Class representing  a Cucumber Step.
 * @author Stephen Charman
 *
 */
public class Step {

	/**
	 * The test for this step.
	 */
	private String mText;
	
	
	/**
	 * Indication of if this step executed successfully.
	 */
	private boolean mSuccessful;
	
	/**
	 * Creates a new instance.
	 * @param text the test for this step.
	 */
	public Step(final String text) {
		mText = text;
	}
	
	/**
	 * Gets the text for this step.
	 * @return the text.
	 */
	public String getText() {
		return mText;
	}

	/**
	 * Sets whether this step has executed successfully.
	 * @param success true if successful; otherwise false.
	 */
	public void setSuccess(final boolean success) {
		mSuccessful = success;
	}
	
	/**
	 * Gets whether this step has executed successfully.
	 * @return true if successful; otherwise false.
	 */
	public boolean isSuccessful() {
		return mSuccessful;
	}
	
	/**
	 * Gets a json representation of this step.
	 * @return the constructed json object.
	 * @throws JSONException if there is an error creating the object.
	 */
	public JSONObject getJson() throws JSONException {
		JSONObject json = new JSONObject();
		json.put("name", mText);
		json.put("success", mSuccessful);
		
		return json;
	}
	
	/** writes an xml line for this step.
	 * @param serializer the serializer to write into.
	 * @throws IOException on an error.
	 */
	public void getXml(final XmlSerializer serializer) throws IOException {
		serializer.startTag("", "failure");
		serializer.attribute("", "message", mText);
		serializer.endTag("", "failure");
	}
}
