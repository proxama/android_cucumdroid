/**
 * This work is licensed under a Creative Commons Attribution-ShareAlike 3.0 Unported License.
 * http://creativecommons.org/licenses/by-sa/3.0/deed.en_GB
**/
package com.proxama.android.cucumdroid.dill.model;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xmlpull.v1.XmlSerializer;

/**
 * Class representing  a Cucumber Scenario.
 * @author Stephen Charman
 *
 */
public class Scenario {
	
	/**
	 * The title for the scenario.
	 */
	private String mTitle;
	
	/**
	 * The {@link Step}s associated with this scenario. 
	 */
	private List<Step> mSteps = new ArrayList<Step>();
	
	/**
	 * Creates a new instance of a Scenario.
	 * @param title the title of this scenario.
	 */
	public Scenario(final String title) {
		mTitle = title;
	}
	
	/**
	 * Get the title.
	 * @return the scenarios title.
	 */
	public String getTitle() {
		return mTitle;
	}
	
	/**
	 * Add a {@link Step} to the scenario.
	 * @param step to be added.
	 */
	public void addStep(final Step step) {
		mSteps.add(step);
	}
	
	/**
	 * Gets all the steps.
	 * @return The {@link Step}s or an empty list.
	 */
	public List<Step> getSteps() {
		return mSteps;
	}
	
	/**
	 * Indicates if all the steps for this scenario have completed successfully.
	 * @return true if all the {@link Step}s are successful.
	 */
	public boolean isSuccessful() {
		boolean success = true;
		for (Step step : mSteps) {
			if (!step.isSuccessful()) {
				success = false;
				break;
			}
		}
		
		return success;
	}
	
	/**
	 * Gets a json representation of this scenario and all it's child {@link Step}s.
	 * @return the constructed json object.
	 * @throws JSONException if tere is an error in building the json.
	 */
	public JSONObject getJson() throws JSONException {
		JSONObject json = new JSONObject();
		json.put("name", mTitle);
		json.put("successful", isSuccessful());
		
		JSONArray steps = new JSONArray();
		for (Step step : mSteps) {
			steps.put(step.getJson());
		}
		
		json.put("steps", steps);
		
		return json;
	}
	
	/** writes an xml line for this scenario.
	 * @param serializer the serializer to write into.
	 * @throws IOException on an error.
	 */
	public void getXml(final XmlSerializer serializer) throws IOException {
		
		serializer.startTag("", "testcase");
		serializer.attribute("", "name", mTitle);
		serializer.attribute("", "status", "run");
		serializer.attribute("", "time", "0");		
		serializer.attribute("", "classname", mTitle);

		for (Step step : mSteps) {
			if (!step.isSuccessful()) {
				step.getXml(serializer);
				break;
			}
		}
		serializer.endTag("", "testcase");
	}
}
